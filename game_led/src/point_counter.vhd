-------------------------------------------------------------------------------
--
-- Title       : point_counter
-- Design      : game_led
-- Author      : 305173
-- Company     : Akademia G�rniczo-Hutnicza im. Stanis�awa Staszica w Krakowie
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\Project_Game\game_led\src\point_counter.vhd
-- Generated   : Fri Sep  3 00:11:10 2021
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {point_counter} architecture {point_counter}}

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;

entity point_counter is
	 port(
		 CLK : in STD_LOGIC;
		 RESET : in STD_LOGIC;
		 WIN : in STD_LOGIC;
		 LOSE : in STD_LOGIC;
		 HARD_MODE : in STD_LOGIC_VECTOR(1 downto 0);
		 D_UNIT : out STD_LOGIC_VECTOR(3 downto 0);
		 D_TENS : out STD_LOGIC_VECTOR(3 downto 0);
		 D_HUNDRED : out STD_LOGIC_VECTOR(3 downto 0);
		 D_THOUSAND : out STD_LOGIC_VECTOR(3 downto 0); 
		 IN_GAME : out STD_LOGIC
	     );
end point_counter;

--}} End of automatically maintained section

architecture point_counter of point_counter is 

signal win_buffer : STD_LOGIC;	
signal lose_buffer : STD_LOGIC;
constant easy_points : integer := 4;
constant normal_points : integer := 15;
constant hard_points : integer := 30; 
signal win_points : integer;
signal lose_points : integer; 	
constant start_points : integer := 10;	
signal actual_points : integer; 
begin 
	
	count_win: process (CLK, RESET) 
		
	begin
		if RESET = '1' then
			win_points <= 0;
		elsif actual_points /= 0 and actual_points /= 9999 then
			
			if win_buffer /= WIN and WIN = '1' then	   --single action 
				if HARD_MODE = "00" then
					win_points <= win_points + easy_points;
				elsif  HARD_MODE = "01" then  
					win_points <= win_points + normal_points;
				elsif  HARD_MODE = "10" then  
					win_points <= win_points + hard_points;
				elsif  HARD_MODE = "11" then  
					win_points <= win_points + easy_points;
				end if;		
					
			end if;	 
			 
			win_buffer <= WIN;
			
		end if;
	
	end process count_win;  
	
	
	count_lose: process (CLK, RESET) 
		
	begin
		if RESET = '1' then
			lose_points <= 0;
		elsif actual_points /= 0 and actual_points /= 9999 then
			
			if lose_buffer /= LOSE and LOSE = '1' then	   --single action 
				lose_points <= lose_points + 1;	
			end if;	 
			 
			lose_buffer <= LOSE;
			
		end if;
	
	end process count_lose; 
 
	
actual_points <= start_points +	win_points - lose_points;

D_UNIT <= std_logic_vector(to_unsigned((actual_points mod 10), 4));
D_TENS <= std_logic_vector(to_unsigned(((actual_points mod 100) / 10), 4));
D_HUNDRED <= std_logic_vector(to_unsigned(((actual_points mod 1000) / 100), 4));
D_THOUSAND <= std_logic_vector(to_unsigned(((actual_points mod 10000) / 1000), 4));	

IN_GAME <= '1' when actual_points > 0 else '0';
	
end point_counter;
