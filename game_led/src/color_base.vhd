-------------------------------------------------------------------------------
--
-- Title       : color_base
-- Design      : Bandit
-- Author      : 305173
-- Company     : Akademia G�rniczo-Hutnicza im. Stanis�awa Staszica w Krakowie
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\Project\Bandit\src\color_base.vhd
-- Generated   : Wed Aug 25 23:31:03 2021
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {color_base} architecture {color_base}}

library IEEE;
use IEEE.std_logic_1164.all; 
use IEEE.STD_LOGIC_UNSIGNED.all;

entity color_base is
	 port(
		 DataIn : in STD_LOGIC_VECTOR(2 downto 0);
		 OutR : out STD_LOGIC_VECTOR(7 downto 0);
		 OutG : out STD_LOGIC_VECTOR(7 downto 0);
		 OutB : out STD_LOGIC_VECTOR(7 downto 0)
	     );
end color_base;

--}} End of automatically maintained section

architecture color_base of color_base is
begin
--Value assignment
	with DataIn select
		OutR <= 	x"FF" when "000",
					x"00" when "001",
					x"00" when "010",
					x"FF" when "011",
					x"FF" when "100",
					x"00" when "101",
					x"FF" when "110",
					x"00" when "111",
					x"00" when others;	 
					
	with DataIn select				
		OutG <= 	x"00" when "000",
					x"FF" when "001",
					x"00" when "010",
					x"FF" when "011",
					x"00" when "100",
					x"FF" when "101",
					x"FF" when "110",
					x"00" when "111",
					x"00" when others;	
					
	with DataIn select				
		OutB <= 	x"00" when "000",
					x"00" when "001",
					x"FF" when "010",
					x"00" when "011",
					x"FF" when "100",
					x"FF" when "101",
					x"FF" when "110",
					x"00" when "111",
					x"00" when others;	 
				
end color_base;
