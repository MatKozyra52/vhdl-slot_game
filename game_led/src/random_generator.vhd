-------------------------------------------------------------------------------
--
-- Title       : random_generator
-- Design      : game_led
-- Author      : 305173
-- Company     : Akademia G�rniczo-Hutnicza im. Stanis�awa Staszica w Krakowie
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\Project_Game\game_led\src\random_generator.vhd
-- Generated   : Thu Sep  2 19:39:53 2021
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {random_generator} architecture {random_generator}}

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity random_generator is
	 port(
		 CLK : in STD_LOGIC;
		 RESET : in STD_LOGIC;
		 GET_VALUE : in STD_LOGIC;	
		 HARD_MODE : in STD_LOGIC_vector(1 DOWNTO 0);
		 VALUE1 : out STD_LOGIC_vector(2 DOWNTO 0);
		 VALUE2 : out STD_LOGIC_vector(2 DOWNTO 0);
		 VALUE3 : out STD_LOGIC_vector(2 DOWNTO 0);
		 WIN : out STD_LOGIC
	     );
end random_generator;

--}} End of automatically maintained section

architecture random_generator of random_generator is

signal MAX_VALUE : integer := 6; 
signal DIVIDER1: std_logic_vector(3 downto 0);	
constant divide_factor1: integer := 11;	
signal DIVIDER2: std_logic_vector(3 downto 0);	
constant divide_factor2: integer := 13;
signal DIVIDER3: std_logic_vector(4 downto 0);	
constant divide_factor3: integer := 17;		 	
signal Qint1 : STD_LOGIC_VECTOR (2 downto 0);	
signal Qint2 : STD_LOGIC_VECTOR (2 downto 0);	
signal Qint3 : STD_LOGIC_VECTOR (2 downto 0);	

begin 
	
MAX_VALUE 	<=	2 when HARD_MODE = "00" else
				4 when HARD_MODE = "01" else
				6 when HARD_MODE = "10" else
				1 when HARD_MODE = "11" else 1;	  
					
	
	process (CLK, RESET)	 
	begin
		if RESET = '1' then

		else
			if CLK'event and CLK = '1' and GET_VALUE = '1' then	
				if HARD_MODE = "11" and Qint1 = Qint2 then 
					if Qint1 = "001" then
						VALUE3 <= "000";
					else
						VALUE3 <= "001"; 
					end if;
					VALUE1 <= Qint1;
					VALUE2 <= Qint2;
					
				else
					VALUE1 <= Qint1;
					VALUE2 <= Qint2;
					VALUE3 <= Qint3;
					if Qint1 = Qint2 and Qint2 = Qint3 then
						WIN <= '1';
					else
						WIN <= '0';
					end if;
				end if;
			end if;
		end if;	

	end process;
	

	
	
	 
	process (CLK, RESET)
	begin
		if RESET = '1' then
			DIVIDER1 <= (others => '0'); 
			Qint1 <= "000";
		elsif CLK'event and CLK = '1' and GET_VALUE = '0' then
			if DIVIDER1 = (divide_factor1-1) then
				DIVIDER1 <= (others => '0');
				if Qint1 < MAX_VALUE then
					Qint1 <= Qint1+1;
				else
					Qint1 <= "000";
				end if;
			else
				DIVIDER1 <= DIVIDER1 + 1;
			end if;
		end if;
	end process;  
	
	process (CLK, RESET)
	begin
		if RESET = '1' then
			DIVIDER2 <= (others => '0');
			Qint2 <= "000";
		elsif CLK'event and CLK = '1' and GET_VALUE = '0' then
			if DIVIDER2 = (divide_factor2-1) then
				DIVIDER2 <= (others => '0');  
				if Qint2 < MAX_VALUE then
					Qint2 <= Qint2+1;
				else
					Qint2 <= "000";
				end if;
			else
				DIVIDER2 <= DIVIDER2 + 1;
			end if;
		end if;
	end process;
	
	process (CLK, RESET)
	begin
		if RESET = '1' then
			DIVIDER3 <= (others => '0');
			Qint3 <= "000";
		elsif CLK'event and CLK = '1' and GET_VALUE = '0' then
			if DIVIDER3 = (divide_factor3-1) then
				DIVIDER3 <= (others => '0');  
				if Qint3 < MAX_VALUE then
					Qint3 <= Qint3+1;
				else
					Qint3 <= "000";
				end if;
			else
				DIVIDER3 <= DIVIDER3 + 1;
			end if;
		end if;
	end process;

end random_generator;
