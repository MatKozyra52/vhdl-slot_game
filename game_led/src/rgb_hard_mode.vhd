-------------------------------------------------------------------------------
--
-- Title       : rgb_hard_mode
-- Design      : game_led
-- Author      : 305173
-- Company     : Akademia G�rniczo-Hutnicza im. Stanis�awa Staszica w Krakowie
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\Project_Game\game_led\src\rgb_hard_mode.vhd
-- Generated   : Sat Sep  4 15:42:07 2021
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {rgb_hard_mode} architecture {rgb_hard_mode}}

library IEEE;
use IEEE.std_logic_1164.all;

entity rgb_hard_mode is
	 port(
		 RESET : in STD_LOGIC;
		 HARD_MODE : in STD_LOGIC_VECTOR(1 downto 0);
		 RGB_LED : out STD_LOGIC_VECTOR(5 downto 0)
	     );
end rgb_hard_mode;

--}} End of automatically maintained section

architecture rgb_hard_mode of rgb_hard_mode is
begin
	
RGB_LED <= 	"000000" when RESET = '1' else
			"000010" when HARD_MODE = "00" else	
			"000110" when HARD_MODE = "01" else
			"000100" when HARD_MODE = "10" else
			"100100" when HARD_MODE = "11";


end rgb_hard_mode;
