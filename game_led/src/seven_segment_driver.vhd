-------------------------------------------------------------------------------
--
-- Title       : seven_segment_driver
-- Design      : game_led
-- Author      : 305173
-- Company     : Akademia G�rniczo-Hutnicza im. Stanis�awa Staszica w Krakowie
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\Project_Game\game_led\src\seven_segment_driver.vhd
-- Generated   : Fri Sep  3 11:46:45 2021
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {seven_segment_driver} architecture {seven_segment_driver}}

library IEEE;
use IEEE.std_logic_1164.all;

entity seven_segment_driver is
	port(
		CLK, RESET : in STD_LOGIC;
		HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7 : in STD_LOGIC_VECTOR(3 downto 0);
		LED : out STD_LOGIC_VECTOR(6 downto 0);	 
		ANODE : out STD_LOGIC_VECTOR(7 downto 0)
		);
end seven_segment_driver;

--}} End of automatically maintained section

architecture seven_segment_driver of seven_segment_driver is
signal local_anode : STD_LOGIC_VECTOR(7 downto 0);	  
signal local_led : STD_LOGIC_VECTOR(6 downto 0); 	 
signal DIVIDER: std_logic_vector(19 downto 0);	-- internal divider register 
constant divide_factor: integer := 100;			-- divide factor user constant 
signal you_lose: std_logic;
begin
	
	-- enter your statements here --
	--HEX-to-seven-segment decoder
	--	HEX:	in 	STD_LOGIC_VECTOR (3 downto 0);
	--	LED:	out	STD_LOGIC_VECTOR (6 downto 0);
	--
	-- segment encoding
	--      0
	--     ---  
	--  5 |   | 1
	--     ---   <- 6
	--  4 |   | 2
	--     ---
	--      3
you_lose <= '1' when HEX0 = "0000" and HEX1 = "0000" and HEX2 = "0000" and HEX3 = "0000" else '0';
	
	process(CLK)
	variable counter : integer;
	begin
		if RESET = '1' then   
			counter := 0;
			local_anode <= "11111111";
		elsif CLK'event and CLK = '1' then	
			counter := counter + 1;
			if counter > 7	then
				counter := 0;
			end if;
			if counter = 0 then
				local_anode <= "11111110"; 
				if you_lose = '1' then
					local_led <= "0000110"; --E
				else	
					case HEX0 is
						when "0000" => local_led <= "1000000";	--0
					 	when "0001" => local_led <= "1111001";	--1
					 	when "0010" => local_led <= "0100100";	--2
					 	when "0011" => local_led <= "0110000";	--3
					 	when "0100" => local_led <= "0011001";	--4
					 	when "0101" => local_led <= "0010010";	--5
					 	when "0110" => local_led <= "0000010";	--6
					 	when "0111" => local_led <= "1111000";	--7
					 	when "1000" => local_led <= "0000000";	--8
					 	when "1001" => local_led <= "0010000";	--9
					 	when others => local_led <= "1111111";	--EMPTY	 	
					end case; 
				end if;
				
			elsif counter = 1 then
				local_anode <= "11111101";	
				if you_lose = '1' then
					local_led <= "0010010";
				else
					case HEX1 is
						when "0000" => local_led <= "1000000";	--0
					 	when "0001" => local_led <= "1111001";	--1
					 	when "0010" => local_led <= "0100100";	--2
					 	when "0011" => local_led <= "0110000";	--3
					 	when "0100" => local_led <= "0011001";	--4
					 	when "0101" => local_led <= "0010010";	--5
					 	when "0110" => local_led <= "0000010";	--6
					 	when "0111" => local_led <= "1111000";	--7
					 	when "1000" => local_led <= "0000000";	--8
					 	when "1001" => local_led <= "0010000";	--9
					 	when others => local_led <= "1111111";	--EMPTY	  
					end case;
				end if;
				
			elsif counter = 2 then
				local_anode <= "11111011";
				if you_lose = '1' then
					local_led <= "1000000";	--O	
				else
					case HEX2 is
						when "0000" => local_led <= "1000000";	--0
					 	when "0001" => local_led <= "1111001";	--1
					 	when "0010" => local_led <= "0100100";	--2
					 	when "0011" => local_led <= "0110000";	--3
					 	when "0100" => local_led <= "0011001";	--4
					 	when "0101" => local_led <= "0010010";	--5
					 	when "0110" => local_led <= "0000010";	--6
					 	when "0111" => local_led <= "1111000";	--7
					 	when "1000" => local_led <= "0000000";	--8
					 	when "1001" => local_led <= "0010000";	--9
					 	when others => local_led <= "1111111";	--EMPTY	
					end case; 
				end if;
				
			elsif counter = 3 then
				local_anode <= "11110111";
				if you_lose = '1' then
					local_led <= "1000111";		--L
				else
					case HEX3 is
						when "0000" => local_led <= "1000000";	--0
					 	when "0001" => local_led <= "1111001";	--1
					 	when "0010" => local_led <= "0100100";	--2
					 	when "0011" => local_led <= "0110000";	--3
					 	when "0100" => local_led <= "0011001";	--4
					 	when "0101" => local_led <= "0010010";	--5
					 	when "0110" => local_led <= "0000010";	--6
					 	when "0111" => local_led <= "1111000";	--7
					 	when "1000" => local_led <= "0000000";	--8
					 	when "1001" => local_led <= "0010000";	--9
					 	when others => local_led <= "1111111";	--EMPTY	  
					end case;
				end if;
				
			elsif counter = 4 then
				local_anode <= "11101111";
				if you_lose = '1' then
					local_led <= "1111111";	  	--space	
				else
					case HEX4 is
						when "0000" => local_led <= "1000000";	--0
					 	when "0001" => local_led <= "1111001";	--1
					 	when "0010" => local_led <= "0100100";	--2
					 	when "0011" => local_led <= "0110000";	--3
					 	when "0100" => local_led <= "0011001";	--4
					 	when "0101" => local_led <= "0010010";	--5
					 	when "0110" => local_led <= "0000010";	--6
					 	when "0111" => local_led <= "1111000";	--7
					 	when "1000" => local_led <= "0000000";	--8
					 	when "1001" => local_led <= "0010000";	--9
					 	when others => local_led <= "1111111";	--EMPTY	 
					end case;
				end if;
				
			elsif counter = 5 then
				local_anode <= "11011111";	
				if you_lose = '1' then
					local_led <= "1000001";  --U
				else
					case HEX5 is
						when "0000" => local_led <= "1000000";	--0
					 	when "0001" => local_led <= "1111001";	--1
					 	when "0010" => local_led <= "0100100";	--2
					 	when "0011" => local_led <= "0110000";	--3
					 	when "0100" => local_led <= "0011001";	--4
					 	when "0101" => local_led <= "0010010";	--5
					 	when "0110" => local_led <= "0000010";	--6
					 	when "0111" => local_led <= "1111000";	--7
					 	when "1000" => local_led <= "0000000";	--8
					 	when "1001" => local_led <= "0010000";	--9
					 	when others => local_led <= "1111111";	--EMPTY	 
					end case;
				end if;
				
			elsif counter = 6 then
				local_anode <= "10111111";	
				if you_lose = '1' then
					local_led <= "1000000";  --O
				else
					case HEX6 is
						when "0000" => local_led <= "1000000";	--0
					 	when "0001" => local_led <= "1111001";	--1
					 	when "0010" => local_led <= "0100100";	--2
					 	when "0011" => local_led <= "0110000";	--3
					 	when "0100" => local_led <= "0011001";	--4
					 	when "0101" => local_led <= "0010010";	--5
					 	when "0110" => local_led <= "0000010";	--6
					 	when "0111" => local_led <= "1111000";	--7
					 	when "1000" => local_led <= "0000000";	--8
					 	when "1001" => local_led <= "0010000";	--9
					 	when others => local_led <= "1111111";	--EMPTY	 
					end case;
				end if;
				
			elsif counter = 7 then
				local_anode <= "01111111";	
				if you_lose = '1' then
					local_led <= "0010001";	  --y
				else
					case HEX7 is
						when "0000" => local_led <= "1000000";	--0
					 	when "0001" => local_led <= "1111001";	--1
					 	when "0010" => local_led <= "0100100";	--2
					 	when "0011" => local_led <= "0110000";	--3
					 	when "0100" => local_led <= "0011001";	--4
					 	when "0101" => local_led <= "0010010";	--5
					 	when "0110" => local_led <= "0000010";	--6
					 	when "0111" => local_led <= "1111000";	--7
					 	when "1000" => local_led <= "0000000";	--8
					 	when "1001" => local_led <= "0010000";	--9
					 	when others => local_led <= "1111111";	--EMPTY	 
					end case;
				end if;
				
			end if;
			
		end if;
		
		
	end process;
	
	ANODE <= local_anode;
	LED <= local_led;
	
end seven_segment_driver;
