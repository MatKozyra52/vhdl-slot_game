-------------------------------------------------------------------------------
--
-- Title       : led_anim
-- Design      : game_led
-- Author      : 305173
-- Company     : Akademia G�rniczo-Hutnicza im. Stanis�awa Staszica w Krakowie
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\Project_Game\game_led\src\led_anim.vhd
-- Generated   : Fri Sep  3 20:34:35 2021
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {led_anim} architecture {led_anim}}

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity led_anim is
	 port(
		 CLK : in STD_LOGIC;
		 RESET : in STD_LOGIC;
		 WIN : in STD_LOGIC;
		 LED_OUT : out STD_LOGIC_VECTOR(15 downto 0)
	     );
end led_anim;

--}} End of automatically maintained section

architecture led_anim of led_anim is  
signal DIVIDER: std_logic_vector(23 downto 0);	
constant divide_factor: integer := 10000000;		 	
signal Qint : STD_LOGIC_VECTOR (15 downto 0);	
begin

	 -- enter your statements here --
	process (CLK, RESET)
	begin
		if RESET = '1' then
			DIVIDER <= (others => '0');
			Qint <= (others => '0');
		elsif CLK'event and CLK = '1' then
			if DIVIDER = (divide_factor-1) then
				DIVIDER <= (others => '0');  
				
				if WIN = '1' then
					if Qint = 0 then
						Qint <= (others => '1');	  
					else
						Qint <= (others => '0');
					end if;
				else
					if Qint(15) = '0' then
						Qint <= Qint(14 downto 0) & '1';  
					else 
						Qint <= Qint(14 downto 0) & '0'; 
					end if;
				end if;
				
			else
				DIVIDER <= DIVIDER + 1;
			end if;
		end if;
	end process;
	
	LED_OUT <= Qint;
	
end led_anim;
