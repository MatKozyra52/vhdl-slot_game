-------------------------------------------------------------------------------
--
-- Title       : ws2812b_driver
-- Design      : game_led
-- Author      : 305173
-- Company     : Akademia G�rniczo-Hutnicza im. Stanis�awa Staszica w Krakowie
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\Project_Game\game_led\src\ws2812b_driver.vhd
-- Generated   : Thu Aug 26 18:36:34 2021
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {ws2812b_driver} architecture {ws2812b_driver}}

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity ws2812b_driver is
	port(
		CLK : in STD_LOGIC;
		LOAD : in STD_LOGIC;
		R1, G1, B1 : in STD_LOGIC_VECTOR(7 downto 0);
		R2, G2, B2 : in STD_LOGIC_VECTOR(7 downto 0);
		R3, G3, B3 : in STD_LOGIC_VECTOR(7 downto 0);
		Dout : out STD_LOGIC
		);
end ws2812b_driver;





architecture ws2812b_driver of ws2812b_driver is	
begin

main: process(CLK)  

variable bits_to_go : integer;
variable data_frame : std_logic_vector(71 downto 0);
variable counter : integer := 0;

begin  

	if CLK'event and CLK = '1' then
		counter := counter + 1;
		if bits_to_go > 0 then
			if counter < 125 then
				if data_frame(data_frame'left) = '1' and counter < 80 then	  	--T1H
					Dout <= '1';
				elsif data_frame(data_frame'left) = '1' and counter > 80 then	--T1L	 
					Dout <= '0';
				elsif data_frame(data_frame'left) = '0' and counter < 40 then	--T0H 
					Dout <= '1';
				elsif data_frame(data_frame'left) = '0' and counter > 40 then	--T0L 
					Dout <= '0'; 
				end if;
			else	--counter greater than 125
				data_frame := data_frame(data_frame'left-1 downto 0) & '0';
				counter := 0;
				bits_to_go := bits_to_go - 1;
			end if;
		
		elsif bits_to_go = 0 then	 
			if counter > 10000 then
				counter := 0;
				bits_to_go := -1;
			end if;
			
		else	--if 0 bits to go
			if LOAD = '1' then	  --if new data to load
				data_frame := 	G1 & R1 & B1 & G2 & R2 & B2 & G3 & R3 & B3;    
				bits_to_go := data_frame'left+1; 
				counter := 0;
			else				--if no new data -- idle state
				counter := 0;
				Dout <= '1';
			end if;
		end if;	
	end if;
	
end process main;



end ws2812b_driver;
